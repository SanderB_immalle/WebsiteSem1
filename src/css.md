# CSS
 
Opgedeeld in:
- Basics
- CSS selectors
- CSS libraries

### Basics

In de `<style></style>` tag:
* `Font-size` -- *Lettergroote*
* `Color` -- *KLeur van de tekst*
* `Bakcground-color` -- Kleur van de achtergrond
* `Border` -- Rand
* ...


### CSS selectors 

Om specifieke elementen of classes op te maken gebruik je selectors.
* `p`, `div`    -- *Tags van elementen*
* `#Vierkant`   -- *Tags voor id's*
* `.vierkant`   -- *Tags voor classes*
* `ul > li`     -- *Tags voor direct kind van ...*

### CSS libraries

Libraries zijn er gemaakt voor het gemakkelijk te maken. Dit zijn vooraf gemaakte CSS's classes.
Het enige nadeel is dat je code wat slordig wordt.

Voorbeelden zijn:
- Pure-css
- Bootstrap
- Foundation

