# Javascript

Opgedeeld in:
- Basis
- Functies
- DOM Manipulatie


### Basis
 De basis van javascript is het gebruik van variabelen (`var`), `for`-loops, `if` en `else`, `console.log`, ...

### Functies

Een functie is een stukje code dat je achteraf kan oproepen. Een basisvoorbeeld is:

```
function zegHallo() {
    console.log("Hallo");
}
```
Het resultaat van deze functie is:
```
Hallo
```
Functies kunnen ook paramters hebben zoals bijvoorbeeld:
```
function zegHallo(naam) {
    console.log("Hallo" + naam);
}
```
Als je de funtie aanroept als `zegHallo("Jan")` is het resultaat:
```
Hallo Jan
```

Je kan ook een functie meegeven als parameter.

### DOM Manipulatie
Bij DOM Manipulatie kan je dingen op je html-page aanpassen
