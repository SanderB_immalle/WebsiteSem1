# HTML
Opgedeeld in:
- Basis
- Elementen
- Linken

Basis
---
De basis van html ? Het maken van `<h1>`, `<h2>`, `<p>`.
Dit zijn basis elementen, met de "h" bedoel je titel, het cijfer dat erbij staat, staat voor de grootte van de titel (`<h1>` staat boven `<h2>`).`<p>` staat voor paragraph. Hier zet je gewoon tekst in.

Elementen
---
We hebben ook andere elementen gezien zoals:
- `<ul>` *(Unordered list)*
- `<ol>` *(Ordered list)*
- `<li>` *(List item)*
- `<th>` *(Table head)*
- `<tb>` *(Table body)*
-  ...
-  
Linken 
---
Je kan bestanden, images, links, ... linken met `<a href="#">`
