﻿var bgc = document.getElementById("changeBg");

bgc.addEventListener('input', bgChanger);

function bgChanger() {
    document.body.style.backgroundColor = bgc.value;
    localStorage.setItem("backgroundColor", bgc.value);
}
function prevBg() {
    document.body.style.backgroundColor = localStorage.getItem('backgroundColor');
}
prevBg();